var isMobile = {
Android: function() {
return navigator.userAgent.match(/Android/i);
},
BlackBerry: function() {
return navigator.userAgent.match(/BlackBerry/i);
},
iOS: function() {
return navigator.userAgent.match(/iPhone|iPad|iPod/i);
},
Opera: function() {
return navigator.userAgent.match(/Opera Mini/i);
},
Windows: function() {
return navigator.userAgent.match(/IEMobile/i);
},
any: function() {
return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
}
};

var startX = -1;

$(document).ready(function() {
   resizer();
   
   
   $(window).resize(function(){
      resizer();
   });




   $('.style-input').focus(function(){
         var def = $(this).attr("defvalue"),
            val = $(this).val();
         if (def===val){
            $(this).val('');

         }
         $(this).addClass('with-val');
      });

   $('.style-input').blur(function(){
      var def = $(this).attr("defvalue"),
         val = $(this).val();
      if (def===val || !val){
         $(this).val(def);
         $(this).removeClass('with-val');

      }
   });

   $('.one-preview').click(function(){
      var sls = $(this).closest('.photo');
      sls.find('.one-preview').removeClass("active");
      $(this).addClass("active");
      sls.find('.big-photo').css("background-image", "url('"+$(this).attr('big')+"')");

      var k = sls.find('.one-preview').eq(0);
      var mt = -parseInt(k.css("margin-top"),10);

      var num = 0;
      for (var i=0;i<sls.find('.one-preview').length;i++){
         if (sls.find('.one-preview').eq(i).hasClass("active"))
            break;
         num++;
      }

      var ab = num*113;
      var cd = 4*113;
      var max = (sls.find('.one-preview').length-4)*113;

      if (mt>=ab || ab>=cd+mt){
         if (ab>max){
            ab = max;
         }
         k.css({"margin-top":-ab+"px"});
      }


   });

   $('.buttons td').click(function(){
      $('.buttons td').removeClass('active');
      $(this).addClass('active');
      $('.tab').hide();
      $('#'+$(this).attr("tabid")).show();
   });

   $('.arr-top, .left-arr').click(function(){
      var sls = $(this).closest('.photo');
      var a = sls.find('.one-preview.active');
      var b = a.prev('.one-preview');
      if (b.length>0){
         b.trigger("click");
      }
   });

   $('.arr-bot, .right-arr').click(function(){
      var sls = $(this).closest('.photo');
      var a = sls.find('.one-preview.active');
      var b = a.next('.one-preview');
      if (b.length>0){
         b.trigger("click");
      }
   });

   $('.submit').click(function(){
      $(this).closest("form").submit();
   });


   $('.button-menu').click(function(ev){
      ev.stopImmediatePropagation();
      $('.inner-menu').show();
      
   });
   
   $('body').click(function(){
      $('.inner-menu').hide();
   });

   var lm = new lmMap();
   lm.init();
});




function validate(form_id,email) {
   
   var address = document.forms[form_id].elements[email].value;
   if(reg.test(address) == false) {
      alert('Введите корректный e-mail');
      return false;
   }
}


function resizer(){
   setTimeout(function(){
      var w = $('body').width(),
         h = $(document).height();
      
      $('.right-car-bg').width(w+"px");
      
      if (w<1024 || isMobile.any()){
         $('body').addClass('adaptive');
         $('.adaptive .photo, .adaptive .big-photo, .left-arr, .right-arr').css({"height":w/1.5+"px"});
      } else {
         $('body').removeClass('adaptive');
         $('.adaptive .photo, .adaptive .big-photo').css({"height":"505px"});
      }
   }, 1);
}



//document.onmousemove = mouseMove;
startX = -1;
document.onmousemove = mouseMove;

function mouseMove(event){
   event = fixEvent(event);
   var newX = event.pageX;//-document.body.scrollTop;
   if (startX === -1)
      startX === newX;

   var w = $('body').width()
      
   $('.right-car').width(w-newX+"px");
   $('.slide-car img').css({"left":newX-65+"px"});
   $('.slide-big-car img').css({"left":newX-315+"px"});
}

function fixEvent(e) {
   // получить объект событие для IE
   e = e || window.event;

   // добавить pageX/pageY для IE
   if ( e.pageX == null && e.clientX != null ) {
      var html = document.documentElement;
      var body = document.body;
      e.pageX = e.clientX + (html && html.scrollLeft || body && body.scrollLeft || 0) - (html.clientLeft || 0);
      e.pageY = e.clientY + (html && html.scrollTop || body && body.scrollTop || 0) - (html.clientTop || 0);
   }

   // добавить which для IE
   if (!e.which && e.button) {
      e.which = (e.button & 1) ? 1 : ( (e.button & 2) ? 3 : ( (e.button & 4) ? 2 : 0 ) );
   }

   return e;
};



