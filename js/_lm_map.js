function lmMap(){
	this.fullMapImg = "http://"+document.domain+"/img/map/lm/lm_main.jpg";

	this.firstPartImg = "http://"+document.domain+"/img/map/lm/lm_1.jpg";
	this.secondPartImg = "http://"+document.domain+"/img/map/lm/lm_2.jpg"; 
	this.thirdPartImg = "http://"+document.domain+"/img/map/lm/lm_3.jpg"; 
	this.fourthPartImg1 = "http://"+document.domain+"/img/map/lm/lm_4.jpg"; 
	this.fourthPartImg2 = "http://"+document.domain+"/img/map/lm/lm_5.jpg";
	
	this.loader = null;
	
	this.mainPage = null;
	this.queryFirst = null;
	this.querySecond = null;
	this.queryThird = null;
	this.queryFourthOne = null;
	this.queryFourthTwo = null;
	
	this.fadeTime = 900;
	
	this.xml = null;
	this.array = null;
	
  this.search = false;
	

	this.init = function(){
		mt_lm = this;
		console.log(mt_lm);
		this.loader = document.createElement("div");
		$(this.loader).addClass("loading").html('<span class="gray_big_text">Загрузка плана:<span class="orange_big_text"> Ладожский маяк</span></span>');
		
		$("#lm_map_js").append(this.loader);
		
		$.post("/xml/ko.xml",function(data){
			mt_lm.xml = data;
			mt_lm.array = $(data).find("uch");
			
			
			mt_lm.createMainPage();
			mt_lm.createFirstQuery();
			mt_lm.createSecondQuery();
			mt_lm.createThirdQuery();
			mt_lm.createFourthPartOneQuery();
			mt_lm.createFourthPartTwoQuery();
			
			tabs = new Image();
			tabs.src = "http://"+document.domain+"/img/map/tabs.png";
			
			tab = new Image();
			tab.src = "http://"+document.domain+"/img/map/big_tab.png";
			
			bubble_bg = new Image();
			bubble_bg.src = "http://"+document.domain+"/img/map/bubble_bg_2.png";
			
			back_bg = new Image();
			back_bg.src = "http://"+document.domain+"/img/map/back_bg.png";
			
			close_bg = new Image();
			close_bg.src = "http://"+document.domain+"/img/map/close_pop.png";
			
			
			
			img = new Image();
			img.src = mt_lm.fullMapImg;
			$(img).load(function(){
				img = new Image();
				img.src = mt_lm.firstPartImg;
				$(img).load(function(){
					img = new Image();
					img.src = mt_lm.secondPartImg;
					$(img).load(function(){
						img = new Image();
						img.src = mt_lm.thirdPartImg;
						$(img).load(function(){
							img = new Image();
							img.src = mt_lm.fourthPartImg1;
							$(img).load(function(){
								img = new Image();
								img.src = mt_lm.fourthPartImg2;
								$(img).load(function(){
									
									$(mt_lm.loader).fadeTo(mt_lm.fadeTime,0,function(){
										$(mt_lm.loader).remove();
										mt_lm.loader = null;
										if(mt_lm.mainPage != null){
											$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,1);
                      if(mt_lm.search){
                        mt_lm.searchCheck();
                      }
										}
									});
									
								});
							});
						});
					});
				});				
			});
		});	
		
	};
  
  this.searchCheck = function() {
    $("#lm_map_js .mini_tablet").css("display","none");
    
    $("#lm_table .location .number").each(function(i,e){
       num = "#uch_lm_"+ ($(e).text()).trim();
       $(num).css("display","block");
    });
    
    if (!this.chekForActiveUchs("#query_first")){
      $(".tablet_1").css("display","none");
    }
    if (!this.chekForActiveUchs("#query_second")){
      $(".tablet_2").css("display","none");
    }
    
    if (!this.chekForActiveUchs("#query_third")){
      $(".tablet_3").css("display","none");
    }
    
    if (!this.chekForActiveUchs("#query_fourth_1")){
      $(".tablet_4").css("display","none");
    }
    
    if (!this.chekForActiveUchs("#query_fourth_2")){
      $(".tablet_5").css("display","none");
    }
  }
  
  this.chekForActiveUchs = function(id){
      var yes = false;
  
      $(id+" .mini_tablet").each(function(i,e){
        if($(e).css("display") == "block"){
          yes = true;
          return false;
        }
      });
      
      return yes;
  }
	
	this.createMainPage = function(){
		this.mainPage = document.createElement("div");
		$(this.mainPage).addClass("container").attr("id","main_page");
		
		div = document.createElement("div");
		$(div).addClass("tablet").addClass("tablet_1").html("Первая<br>очередь<br><span class='small_text'>Вторичные продажи</span>").click(function(){
			$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.mainPage).css("display","none");
				$(mt_lm.queryFirst).fadeTo(mt_lm.fadeTime,1);
			});
		});
    
		$(this.mainPage).append(div);
		
		div = document.createElement("div");
		$(div).addClass("tablet").addClass("tablet_2").html("Вторая<br>очередь<br><span class='small_text'>в продаже</span>").click(function(){
			$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.mainPage).css("display","none");
				$(mt_lm.querySecond).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.mainPage).append(div);
		
		div = document.createElement("div");
		$(div).addClass("tablet").addClass("tablet_3").html("Третья<br>очередь<br><span class='small_text'>В продаже</span>").click(function(){
			$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.mainPage).css("display","none");
				$(mt_lm.queryThird).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.mainPage).append(div);
		
		div = document.createElement("div");
		$(div).addClass("tablet").addClass("tablet_4").html("Четвертая<br>очередь-1<br><span class='small_text'>В продаже</span>").click(function(){
			$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.mainPage).css("display","none");
				$(mt_lm.queryFourthOne).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.mainPage).append(div);
		
		div = document.createElement("div");
		$(div).addClass("tablet").addClass("tablet_5").html("Четвертая<br>очередь-2<br><span class='small_text'>В продаже</span>").click(function(){
			$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.mainPage).css("display","none");
				$(mt_lm.queryFourthTwo).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.mainPage).append(div);
		
		$("#lm_map_js").append(this.mainPage);
		
	};
	
	
	
	this.createFirstQuery = function(){
		mt = this;
		
		this.queryFirst = document.createElement("div");
		$(this.queryFirst).addClass("container").attr("id","query_first");
		
		div = document.createElement("div");
		$(div).addClass("back").html("Вернуться к общему плану").click(function(){
			$(mt_lm.queryFirst).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.queryFirst).css("display","none");
				$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.queryFirst).append(div);

		this.createUchs(137,207,mt_lm.queryFirst);

		this.createAUchs("sold","139a",mt_lm.queryFirst);
		this.createAUchs("sold","141a",mt_lm.queryFirst);
		this.createAUchs("sold","142a",mt_lm.queryFirst);
		this.createAUchs("sold","143a",mt_lm.queryFirst);
		this.createAUchs("sold","146a",mt_lm.queryFirst);
		
		$("#lm_map_js").append(this.queryFirst);
	};
	
	this.createSecondQuery = function(){
		this.querySecond = document.createElement("div");
		$(this.querySecond).addClass("container").attr("id","query_second");
		
		mt = this;
		div = document.createElement("div");
		$(div).addClass("back").html("Вернуться к общему плану").click(function(){
			$(mt_lm.querySecond).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.querySecond).css("display","none");
				$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.querySecond).append(div);
		
		
		this.createUchs(1,22,mt_lm.querySecond);
		this.createUchs(24,133,mt_lm.querySecond);
		this.createUchs(135,136,mt_lm.querySecond);
		
		$("#lm_map_js").append(this.querySecond);
	};
	this.createThirdQuery = function(){
		this.queryThird = document.createElement("div");
		$(this.queryThird).addClass("container").attr("id","query_third");
		
		mt = this;
		div = document.createElement("div");
		$(div).addClass("back").html("Вернуться к общему плану").click(function(){
			$(mt_lm.queryThird).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.queryThird).css("display","none");
				$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.queryThird).append(div);
		
		this.createUchs(208,269,mt_lm.queryThird);
		
		$("#lm_map_js").append(this.queryThird);
	};
	
	this.createFourthPartOneQuery = function(){
		this.queryFourthOne = document.createElement("div");
		$(this.queryFourthOne).addClass("container").attr("id","query_fourth_1");
		
		mt = this;
		div = document.createElement("div");
		$(div).addClass("back").html("Вернуться к общему плану").click(function(){
			$(mt_lm.queryFourthOne).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.queryFourthOne).css("display","none");
				$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.queryFourthOne).append(div);
		
		//this.createUchs(270,297,mt_lm.queryFourthOne);
		//this.createUchs(337,349,mt_lm.queryFourthOne);
		this.createUchs(319,361,mt_lm.queryFourthOne);
		
		$("#lm_map_js").append(this.queryFourthOne);
	};
	
	this.createFourthPartTwoQuery = function(){
		this.queryFourthTwo = document.createElement("div");
		$(this.queryFourthTwo).addClass("container").attr("id","query_fourth_2");
		
		mt = this;
		div = document.createElement("div");
		$(div).addClass("back").html("Вернуться к общему плану").click(function(){
			$(mt_lm.queryFourthTwo).fadeTo(mt_lm.fadeTime,0,function(){
				$(mt_lm.queryFourthTwo).css("display","none");
				$(mt_lm.mainPage).fadeTo(mt_lm.fadeTime,1);
			});
		});
		$(this.queryFourthTwo).append(div);
		
		this.createUchs(270,318,mt_lm.queryFourthTwo);

		
		$("#lm_map_js").append(this.queryFourthTwo);
	};
	
	this.createAUchs = function(classText,id,q){
		tablett = document.createElement("div");
		$(tablett).addClass("mini_tablet").addClass(classText).attr("id","uch_lm_"+id).html(id);
		
		$(q).append(tablett);
	};
	
	this.createUchs = function(from,to,elem) {
		mt = this;
	
		for(i=from;i<=to;i++){
		
			div = document.createElement("div");
			$(div).addClass("mini_tablet").attr("id","uch_lm_"+i).html(i);
			$(div).click(function(){
				mt_lm.showBubble(this,elem);
			});
			
			bubble = document.createElement("div");
			$(bubble).addClass("bubble");
			
			str = "";
			str += "<div class='number'><span>Номер участка: </span>"+i+"</div>";
			str += "<div><span>Стоимость сотки (руб.): </span>"+$(mt_lm.array[i]).find("sot").text()+"</div>";
			str += "<div><span>Стоимость (руб.): </span>"+$(mt_lm.array[i]).find("price").text()+"</div>";
      str += "<div><span>Площадь (м.кв.): </span>"+$(mt_lm.array[i]).find("size").text()+"</div>";
			
			tmp = $(mt_lm.array[i]).find("status").text()
			if ( tmp == "n"){
				str += "<div><span>Статус: </span>"+"Снят с продаж"+"</div>";
				$(div).addClass("out_of_sale");
			} else {
				if (tmp == "s"){
					str += "<div><span>Статус: </span>"+"Продан"+"</div>";
					$(div).addClass("sold");
				} else {
					if (tmp == "r"){
						str += "<div><span>Статус: </span>"+"Забронирован"+"</div>";
						$(div).addClass("reserved");
					} else {
						//str += "<div><span>Статус:</span></td><td>"+tmp+"</div>";	
						str += "<div><span>Статус: </span>"+"Свободен"+"</div>";
					}
				}
			}
			if (['n', 's', 'r'].indexOf(tmp) == -1) {
				str += "<div class='more'><a href='http://city78zn.ru/"+$(mt_lm.array[i]).find("url").text()+"'>Подробнее</a></span></div>";	
			}
			$(bubble).addClass("bubble").html(str);
			
			
			$(div).append(bubble);
			
			$(elem).append(div);
		}
	}
	
	this.showBubble = function(obj,bg){
		
		pop = document.createElement("div");
		$(pop).addClass("about_popup").attr("id","about_pop");
		
		$("#about_pop").remove();
		
		cc = document.createElement("div");
		$(cc).addClass("close").click(function(){
			$(this).parents(".about_popup").css("display","none");
			$(this).remove();
		});
		
		$(pop).html($(obj).find(".bubble").html()).append(cc);
		$(bg).append(pop);
	};
	
}